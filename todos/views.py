from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list_list": lists,
    }
    return render(request, "todolist/todolist.html", context)


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": list_detail,
    }
    return render(request, "todolist/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list_list = form.save()
            return redirect("todo_list_detail", id=todo_list_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todolist/create.html", context)


def todo_list_update(request, id):
    model_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=model_instance)
        context = {"form": form}
        return render(request, "todolist/update.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todolist/delete.html")
